-- MySQL dump 10.13  Distrib 5.7.15, for Linux (x86_64)
--
-- Host: localhost    Database: one50
-- ------------------------------------------------------
-- Server version	5.7.15-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `flow_doctrine_migrationstatus`
--

DROP TABLE IF EXISTS `flow_doctrine_migrationstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flow_doctrine_migrationstatus` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flow_doctrine_migrationstatus`
--

LOCK TABLES `flow_doctrine_migrationstatus` WRITE;
/*!40000 ALTER TABLE `flow_doctrine_migrationstatus` DISABLE KEYS */;
INSERT INTO `flow_doctrine_migrationstatus` VALUES ('20110613223837'),('20110613224537'),('20110714212900'),('20110824124835'),('20110824124935'),('20110920125736'),('20110923125535'),('20110923125536'),('20120328152041'),('20120329220340'),('20120329220341'),('20120412093748'),('20120429213445'),('20120429213446'),('20120520211354'),('20120521125401'),('20120525141545'),('20120625211647'),('20120930203452'),('20120930211542'),('20130319131400'),('20141015125841'),('20141113173712'),('20150129225152'),('20150206113911'),('20150206114820'),('20150217145853'),('20150309181635'),('20150309181636'),('20150611154419'),('20150612093351'),('20151110113650'),('20160601164332'),('20160922081426'),('20160922151702'),('20160922201100'),('20160924101025'),('20160924111708'),('20160924162219'),('20160925113940'),('20160927142519'),('20160927143409');
/*!40000 ALTER TABLE `flow_doctrine_migrationstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one50_shop_domain_model_address`
--

DROP TABLE IF EXISTS `one50_shop_domain_model_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one50_shop_domain_model_address` (
  `persistence_object_identifier` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `streetnumber` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` int(11) NOT NULL,
  `city` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  KEY `IDX_3D020FB78D93D649` (`user`),
  CONSTRAINT `FK_3D020FB78D93D649` FOREIGN KEY (`user`) REFERENCES `one50_shop_domain_model_user` (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one50_shop_domain_model_address`
--

LOCK TABLES `one50_shop_domain_model_address` WRITE;
/*!40000 ALTER TABLE `one50_shop_domain_model_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `one50_shop_domain_model_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one50_shop_domain_model_cart`
--

DROP TABLE IF EXISTS `one50_shop_domain_model_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one50_shop_domain_model_cart` (
  `persistence_object_identifier` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  UNIQUE KEY `UNIQ_D213B8878D93D649` (`user`),
  CONSTRAINT `FK_D213B8878D93D649` FOREIGN KEY (`user`) REFERENCES `one50_shop_domain_model_user` (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one50_shop_domain_model_cart`
--

LOCK TABLES `one50_shop_domain_model_cart` WRITE;
/*!40000 ALTER TABLE `one50_shop_domain_model_cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `one50_shop_domain_model_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one50_shop_domain_model_cartitem`
--

DROP TABLE IF EXISTS `one50_shop_domain_model_cartitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one50_shop_domain_model_cartitem` (
  `persistence_object_identifier` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `cart` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  KEY `IDX_ADEAF65EBA388B7` (`cart`),
  KEY `IDX_ADEAF65ED34A04AD` (`product`),
  CONSTRAINT `FK_ADEAF65EBA388B7` FOREIGN KEY (`cart`) REFERENCES `one50_shop_domain_model_cart` (`persistence_object_identifier`),
  CONSTRAINT `FK_ADEAF65ED34A04AD` FOREIGN KEY (`product`) REFERENCES `one50_shop_domain_model_product` (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one50_shop_domain_model_cartitem`
--

LOCK TABLES `one50_shop_domain_model_cartitem` WRITE;
/*!40000 ALTER TABLE `one50_shop_domain_model_cartitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `one50_shop_domain_model_cartitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `one50_shop_domain_model_category`
--

DROP TABLE IF EXISTS `one50_shop_domain_model_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one50_shop_domain_model_category` (
  `persistence_object_identifier` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1023) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `one50_shop_domain_model_order`
--

DROP TABLE IF EXISTS `one50_shop_domain_model_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one50_shop_domain_model_order` (
  `persistence_object_identifier` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transactionid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `billingaddress` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deliveryaddress` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orderdate` datetime NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  KEY `IDX_D32913048D93D649` (`user`),
  KEY `IDX_D3291304B1A7AB52` (`billingaddress`),
  KEY `IDX_D3291304EA6C718C` (`deliveryaddress`),
  CONSTRAINT `FK_D32913048D93D649` FOREIGN KEY (`user`) REFERENCES `one50_shop_domain_model_user` (`persistence_object_identifier`),
  CONSTRAINT `FK_D3291304B1A7AB52` FOREIGN KEY (`billingaddress`) REFERENCES `one50_shop_domain_model_address` (`persistence_object_identifier`),
  CONSTRAINT `FK_D3291304EA6C718C` FOREIGN KEY (`deliveryaddress`) REFERENCES `one50_shop_domain_model_address` (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `one50_shop_domain_model_orderitem`
--

DROP TABLE IF EXISTS `one50_shop_domain_model_orderitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one50_shop_domain_model_orderitem` (
  `persistence_object_identifier` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `parentorder` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  KEY `IDX_D585B3E5D34A04AD` (`product`),
  KEY `IDX_D585B3E595EFD537` (`parentorder`),
  CONSTRAINT `FK_D585B3E595EFD537` FOREIGN KEY (`parentorder`) REFERENCES `one50_shop_domain_model_order` (`persistence_object_identifier`),
  CONSTRAINT `FK_D585B3E5D34A04AD` FOREIGN KEY (`product`) REFERENCES `one50_shop_domain_model_product` (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `one50_shop_domain_model_product`
--

DROP TABLE IF EXISTS `one50_shop_domain_model_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one50_shop_domain_model_product` (
  `persistence_object_identifier` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1023) COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `image` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  UNIQUE KEY `UNIQ_E306649BC53D045F` (`image`),
  KEY `IDX_E306649B64C19C1` (`category`),
  CONSTRAINT `FK_E306649B64C19C1` FOREIGN KEY (`category`) REFERENCES `one50_shop_domain_model_category` (`persistence_object_identifier`),
  CONSTRAINT `FK_E306649BC53D045F` FOREIGN KEY (`image`) REFERENCES `typo3_flow_resource_resource` (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `one50_shop_domain_model_user`
--

DROP TABLE IF EXISTS `one50_shop_domain_model_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `one50_shop_domain_model_user` (
  `persistence_object_identifier` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  CONSTRAINT `FK_5423E67947A46B0A` FOREIGN KEY (`persistence_object_identifier`) REFERENCES `typo3_party_domain_model_abstractparty` (`persistence_object_identifier`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `one50_shop_domain_model_user`
--

LOCK TABLES `one50_shop_domain_model_user` WRITE;
/*!40000 ALTER TABLE `one50_shop_domain_model_user` DISABLE KEYS */;
INSERT INTO `one50_shop_domain_model_user` VALUES ('cfafb3b6-a80e-43a6-aa89-a78aa7202f18','Admin','User');
/*!40000 ALTER TABLE `one50_shop_domain_model_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typo3_flow_mvc_routing_objectpathmapping`
--

DROP TABLE IF EXISTS `typo3_flow_mvc_routing_objectpathmapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typo3_flow_mvc_routing_objectpathmapping` (
  `objecttype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uripattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pathsegment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`objecttype`,`uripattern`,`pathsegment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typo3_flow_mvc_routing_objectpathmapping`
--

LOCK TABLES `typo3_flow_mvc_routing_objectpathmapping` WRITE;
/*!40000 ALTER TABLE `typo3_flow_mvc_routing_objectpathmapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `typo3_flow_mvc_routing_objectpathmapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typo3_flow_resource_resource`
--

DROP TABLE IF EXISTS `typo3_flow_resource_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typo3_flow_resource_resource` (
  `persistence_object_identifier` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `sha1` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `md5` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `collectionname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mediatype` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `relativepublicationpath` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `filesize` decimal(20,0) NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `typo3_flow_security_account`
--

DROP TABLE IF EXISTS `typo3_flow_security_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typo3_flow_security_account` (
  `persistence_object_identifier` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `accountidentifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `authenticationprovidername` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `credentialssource` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creationdate` datetime NOT NULL,
  `expirationdate` datetime DEFAULT NULL,
  `roleidentifiers` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:simple_array)',
  `lastsuccessfulauthenticationdate` datetime DEFAULT NULL,
  `failedauthenticationcount` int(11) DEFAULT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  UNIQUE KEY `flow_identity_typo3_flow_security_account` (`accountidentifier`,`authenticationprovidername`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typo3_flow_security_account`
--

LOCK TABLES `typo3_flow_security_account` WRITE;
/*!40000 ALTER TABLE `typo3_flow_security_account` DISABLE KEYS */;
INSERT INTO `typo3_flow_security_account` VALUES ('3f8c6866-4561-4f43-99c0-07440810ede4','admin','DefaultProvider','bcrypt=>$2a$14$TYFsFEreqO.rgVe063yFGOXiDyFYxJSrBumLoS.Bc8pr1d1Urhq.e','2016-10-06 07:20:45',NULL,'One50.Shop:Admin','2016-10-06 07:51:09',0);
/*!40000 ALTER TABLE `typo3_flow_security_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typo3_party_domain_model_abstractparty`
--

DROP TABLE IF EXISTS `typo3_party_domain_model_abstractparty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typo3_party_domain_model_abstractparty` (
  `persistence_object_identifier` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `dtype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typo3_party_domain_model_abstractparty`
--

LOCK TABLES `typo3_party_domain_model_abstractparty` WRITE;
/*!40000 ALTER TABLE `typo3_party_domain_model_abstractparty` DISABLE KEYS */;
INSERT INTO `typo3_party_domain_model_abstractparty` VALUES ('cfafb3b6-a80e-43a6-aa89-a78aa7202f18','one50_shop_user');
/*!40000 ALTER TABLE `typo3_party_domain_model_abstractparty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typo3_party_domain_model_abstractparty_accounts_join`
--

DROP TABLE IF EXISTS `typo3_party_domain_model_abstractparty_accounts_join`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typo3_party_domain_model_abstractparty_accounts_join` (
  `party_abstractparty` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `flow_security_account` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`party_abstractparty`,`flow_security_account`),
  UNIQUE KEY `UNIQ_1EEEBC2F58842EFC` (`flow_security_account`),
  KEY `IDX_1EEEBC2F38110E12` (`party_abstractparty`),
  CONSTRAINT `FK_1EEEBC2F38110E12` FOREIGN KEY (`party_abstractparty`) REFERENCES `typo3_party_domain_model_abstractparty` (`persistence_object_identifier`),
  CONSTRAINT `FK_1EEEBC2F58842EFC` FOREIGN KEY (`flow_security_account`) REFERENCES `typo3_flow_security_account` (`persistence_object_identifier`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typo3_party_domain_model_abstractparty_accounts_join`
--

LOCK TABLES `typo3_party_domain_model_abstractparty_accounts_join` WRITE;
/*!40000 ALTER TABLE `typo3_party_domain_model_abstractparty_accounts_join` DISABLE KEYS */;
INSERT INTO `typo3_party_domain_model_abstractparty_accounts_join` VALUES ('cfafb3b6-a80e-43a6-aa89-a78aa7202f18','3f8c6866-4561-4f43-99c0-07440810ede4');
/*!40000 ALTER TABLE `typo3_party_domain_model_abstractparty_accounts_join` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typo3_party_domain_model_electronicaddress`
--

DROP TABLE IF EXISTS `typo3_party_domain_model_electronicaddress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typo3_party_domain_model_electronicaddress` (
  `persistence_object_identifier` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `usagetype` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approved` tinyint(1) NOT NULL,
  `dtype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typo3_party_domain_model_electronicaddress`
--

LOCK TABLES `typo3_party_domain_model_electronicaddress` WRITE;
/*!40000 ALTER TABLE `typo3_party_domain_model_electronicaddress` DISABLE KEYS */;
/*!40000 ALTER TABLE `typo3_party_domain_model_electronicaddress` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typo3_party_domain_model_person`
--

DROP TABLE IF EXISTS `typo3_party_domain_model_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typo3_party_domain_model_person` (
  `persistence_object_identifier` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `primaryelectronicaddress` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`persistence_object_identifier`),
  UNIQUE KEY `UNIQ_C60479E15E237E06` (`name`),
  KEY `IDX_C60479E1A7CECF13` (`primaryelectronicaddress`),
  CONSTRAINT `typo3_party_domain_model_person_ibfk_1` FOREIGN KEY (`name`) REFERENCES `typo3_party_domain_model_personname` (`persistence_object_identifier`),
  CONSTRAINT `typo3_party_domain_model_person_ibfk_2` FOREIGN KEY (`primaryelectronicaddress`) REFERENCES `typo3_party_domain_model_electronicaddress` (`persistence_object_identifier`),
  CONSTRAINT `typo3_party_domain_model_person_ibfk_3` FOREIGN KEY (`persistence_object_identifier`) REFERENCES `typo3_party_domain_model_abstractparty` (`persistence_object_identifier`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typo3_party_domain_model_person`
--

LOCK TABLES `typo3_party_domain_model_person` WRITE;
/*!40000 ALTER TABLE `typo3_party_domain_model_person` DISABLE KEYS */;
/*!40000 ALTER TABLE `typo3_party_domain_model_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typo3_party_domain_model_person_electronicaddresses_join`
--

DROP TABLE IF EXISTS `typo3_party_domain_model_person_electronicaddresses_join`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typo3_party_domain_model_person_electronicaddresses_join` (
  `party_person` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `party_electronicaddress` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`party_person`,`party_electronicaddress`),
  KEY `IDX_BE7D49F772AAAA2F` (`party_person`),
  KEY `IDX_BE7D49F7B06BD60D` (`party_electronicaddress`),
  CONSTRAINT `typo3_party_domain_model_person_electronicaddresses_join_ibfk_1` FOREIGN KEY (`party_person`) REFERENCES `typo3_party_domain_model_person` (`persistence_object_identifier`),
  CONSTRAINT `typo3_party_domain_model_person_electronicaddresses_join_ibfk_2` FOREIGN KEY (`party_electronicaddress`) REFERENCES `typo3_party_domain_model_electronicaddress` (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typo3_party_domain_model_person_electronicaddresses_join`
--

LOCK TABLES `typo3_party_domain_model_person_electronicaddresses_join` WRITE;
/*!40000 ALTER TABLE `typo3_party_domain_model_person_electronicaddresses_join` DISABLE KEYS */;
/*!40000 ALTER TABLE `typo3_party_domain_model_person_electronicaddresses_join` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typo3_party_domain_model_personname`
--

DROP TABLE IF EXISTS `typo3_party_domain_model_personname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typo3_party_domain_model_personname` (
  `persistence_object_identifier` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middlename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `othername` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dtype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`persistence_object_identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typo3_party_domain_model_personname`
--

LOCK TABLES `typo3_party_domain_model_personname` WRITE;
/*!40000 ALTER TABLE `typo3_party_domain_model_personname` DISABLE KEYS */;
/*!40000 ALTER TABLE `typo3_party_domain_model_personname` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-12 19:00:00
