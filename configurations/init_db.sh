#!/bin/bash
# Start the MySQL daemon in the background.
echo "Starting MySQL...."
/usr/sbin/mysqld &
mysql_pid=$!

until mysqladmin ping >/dev/null 2>&1; do
  echo -n "."; sleep 0.2
done

echo "Creating One50 Database..."
mysql -u root -pmodul150 -e "CREATE DATABASE one50"

#create database schema
./var/www/html/one50_build/flow doctrine:migrate

#add admin user
./var/www/html/one50_build/flow user:create --username admin --password modul150 --isAdmin true --firstName Admin --lastName Administrator

#create demo-data in database
./var/www/html/one50_build/flow shopdata:import
