FROM debian:jessie
MAINTAINER Lukas Weber
 
RUN apt-get update

#install some basics
RUN apt-get -yqq install \
	apt-utils \
	nano \
	git \
	curl

#pre-set db-password to prevent pw-prompt while installing
RUN echo "mysql-server mysql-server/root_password password modul150" | debconf-set-selections
RUN echo "mysql-server mysql-server/root_password_again password modul150" | debconf-set-selections

#install web and database tools
RUN apt-get -yqq install \
	mysql-server-5.5 \
	php5 \
	libapache2-mod-php5 \ 
	php5-mcrypt \
	php5-fpm \
	php5-mysql \
	mysql-client-5.5 \
	apache2 

#--------------------Web Setup-----------------------

#copy apache config to target
COPY configurations/apache.conf /etc/apache2/sites-available/one50.conf
RUN a2ensite one50

#install composer
RUN curl -sS https://getcomposer.org/installer -o composer-setup.php
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer

#Download one50-project and compose
RUN mkdir /var/www/html/one50_build 
RUN git clone https://devlukas@bitbucket.org/devlukas/gibz-modul-150.git /var/www/html/one50_build
RUN cd /var/www/html/one50_build && composer update && composer install

#copy configs
COPY configurations/Routes.yaml /var/www/html/one50_build/Configuration/Routes.yaml
COPY configurations/Settings.yaml /var/www/html/one50_build/Configuration/Settings.yaml
COPY configurations/Settings.yaml /var/www/html/one50_build/Configuration/Development/Settings.yaml

#--------------------MySQL Setup-----------------------

#Make mysql acessible from outside
RUN sed -i "s/.*bind-address.*/#bind-address = 0.0.0.0/" /etc/mysql/my.cnf 
RUN service mysql restart

#Create Database
ADD configurations/dump.sql /tmp/dump.sql
ADD configurations/init_db.sh /tmp/init_db.sh
RUN ./tmp/init_db.sh

# expose MySQL and HTTP-port
EXPOSE 3306
EXPOSE 80
EXPOSE 443

#set permissions
RUN chown -R www-data /var/www/
 
# start container
ENTRYPOINT \
	#rename the builded app to the configured web-path
	cp -a -n /var/www/html/one50_build/. /var/www/html/one50/ && \
	#update one50-shop
	cd /var/www/html/one50/ && composer update && \
	#start servics and open bash
	service apache2 start && \
	service mysql start && \
	/bin/bash
