if [ -z ${1+x} ]; then
	echo "Please use the script like this ./start.sh <docker-image-id>";
else
	echo "Starting One50-Shop-Container..."
	#sudo chown -R $USER $(pwd)/app/
	sudo docker run -p 80:80 -p 3306:3306 -v $(pwd)/app:/var/www/html/one50 -it $1
fi
